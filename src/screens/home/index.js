import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import ItemCart from '../../components/itemCart';
import LogoTitle from '../../components/header/index'
let img1 = require('../../assets/img/logo.png')
let img2 = require('../../assets/img/logo_Ciao.png')
class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isFirstTab: true
    }
  }
  static navigationOptions = {
    title: 'Đặt hàng',
  }
  changeTab(index) {
    switch (index) {
      case 0:
        this.setState({ isFirstTab: true });
        break;
      case 1:
        this.setState({ isFirstTab: false });
        break;
      default:
        break;
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
      <View style ={{height:80}}>
          <LogoTitle logo1 ={img1} logo2 ={img2}/>
      </View>
        <View style={{ paddingHorizontal: 16, height: 50, flexDirection: 'row', paddingBottom: 5, paddingTop: 5 }}>
          <TouchableOpacity onPress={() => this.changeTab(0)} style={{ backgroundColor: this.state.isFirstTab ? '#003366' : 'grey', flex: 1, borderBottomLeftRadius: 8, borderTopLeftRadius: 8, justifyContent: 'center' }}>
            <Text style={{ color: this.state.isFirstTab ? 'white' : '#1C1C1C', textAlign: 'center', fontWeight: 'bold' }}>Favorites</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.changeTab(1)} style={{ backgroundColor: !this.state.isFirstTab ? '#003366' : 'grey', flex: 1, borderBottomRightRadius: 8, borderTopRightRadius: 8, justifyContent: 'center' }}>
            <Text style={{ color: !this.state.isFirstTab ? 'white' : '#1C1C1C', textAlign: 'center', fontWeight: 'bold' }}>Full menu</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          {
            this.state.isFirstTab
              ? <View style={{ flex: 1, backgroundColor: '#eeeeee' }}>
                <ScrollView>
                  <ItemCart />
                  <ItemCart />
                  <ItemCart />
                  <ItemCart />
                  <ItemCart />
                  <ItemCart />
                  <ItemCart />
                </ScrollView>

              </View>
              : <View style={{ flex: 1, backgroundColor: '#eeeeee' }}>
                {/* <View style={{ flexDirection: 'row', height: 120 }}>
                  <View style ={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image source={require('../../assets/img/logo.png')} style ={{height:70,width:70}} />
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/img/logo_Ciao.png')} style={{ height: 70, width: 70 }} />
                  </View>
                </View> */}
              </View>
          }
        </View>
      </View>
    )
  }
}
export default HomeScreen;