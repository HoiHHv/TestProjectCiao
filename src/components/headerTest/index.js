import React from 'react';
import { TouchableOpacity, Image, View, Text } from 'react-native';
import { DrawerActions, withNavigation } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';

class HeaderTest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSetting: false
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isSetting !== this.state.isSetting) {
      this.setState({ isSetting: nextProps.isSetting });
    }
  }
  toggleDrawer() {
    this.props.navigation.dispatch(DrawerActions.toggleDrawer())
  }
  render() {
    return (
      <View style={{ flex: 1, height: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', alignItems: 'center', borderBottomColor: 'grey', borderBottomWidth: 1 }}>
        <TouchableOpacity onPress={() => this.toggleDrawer()} style={{ width: 60, padding: 15 }}>
          <Ionicons name='md-menu' size={24} color='black' />
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', flex: 1, height: '100%' }}>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#D3D3D3', }}>
            <Image
              source={this.props.logo1}
              style={{ width: 40, height: 60 }}
            />
          </TouchableOpacity>
          <View style={{ width: 60 }}>
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(HeaderTest);