import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
let iconName = 'ios-cart';
let iconHis = 'ios-calendar'
class MenuScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstMenu: true
    }
  }
  render() {
    return (
      <View style={{ flex: 1, }}>

        <ImageBackground style={{ height: 200, }} source={require('../../assets/img/headerLeft.jpg')}>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Image source={require('../../assets/img/girl.jpg')} style={{ height: 90, width: 90, borderRadius: 45 }} />
            <Text style={{ marginTop: 5, color: 'white', fontWeight: 'bold' }}>Chào : Admin</Text>
            <View style={{ flexDirection: 'row', borderTopColor: 'white', borderTopWidth: 1, width: '70%' }}>
              <View style={{ flex: 2 }}>
                <Text style={{ marginTop: 5, color: 'white', fontWeight: 'bold' }} >Id</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>123456</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', width: '70%' }}>
              <View style={{ flex: 2 }}>
                <Text style={{ marginTop: 5, color: 'white', fontWeight: 'bold' }} >Tài khoản</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>69.9669 đ</Text>
              </View>
            </View>
          </View>
        </ImageBackground>
        {/* phần menu phía dưới */}
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: this.state.isFirstMenu ? '#003366' : '#F5F5F5', }}
          >
            <Ionicons name={iconName} size={25} style={{ marginLeft: 20 }} color='white' />
            <Text style={style.textMenu} style={{ fontWeight: 'bold', color: 'white', marginLeft: 20}}>ĐẶT HÀNG</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name={iconHis} size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>LỊCH SỬ ĐẶT HÀNG</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name='md-card' size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>VOUCHER KHUYẾN MÃI</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name='ios-cash' size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>NẠP TIỀN</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name='ios-contact' size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>TÀI KHOẢN</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name='md-settings' size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>CÀI ĐẶT</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor: !this.state.isFirstMenu ? '#003366' : '#F5F5F5', }} style={style.menu}>
            <Ionicons name='md-log-out' size={25} style={{ marginLeft: 20 }} color='#003366' />
            <Text style={style.textMenu}>ĐĂNG XUẤT</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const style = StyleSheet.create(
  {
    menu: {
      flex: 1, flexDirection: 'row', alignItems: 'center',

    },
    textMenu: {
      fontWeight: 'bold', color: '#003366', marginLeft: 20
    }
  }
)
export default MenuScreen;