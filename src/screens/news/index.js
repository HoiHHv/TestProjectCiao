import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import LogoTitle from '../../components/header/index'
import HeaderTest from '../../components/headerTest'
let img1 = require('../../assets/img/logo.png')
class NewsScreen extends Component {
  constructor(props) {
    super(props)
  }
  static navigationOptions = {
    title: 'Tin mới'
  }
  render() {
    return (
      <View style={{ flex: 1, }}>
        <View style={{ height: 80 }}>
        
          <HeaderTest  logo1 = {img1}/>
        </View>
        <View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center',flex:1 }}>
          <Text>This is NewsScreen</Text>
        </View>
        
      </View>
    )
  }
}
export default NewsScreen;