import React from 'react';
import { TouchableOpacity, Image, View, Text } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  DrawerActions,
  withNavigation,
  StackNavigator
}
  from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LogoTitle from './components/header/index';
import HomeScreen from './screens/home';
import HistoryScreeen from './screens/history';
import NewsScreen from './screens/news';
import UserScreen from './screens/user';
import InfomationScreen from './screens/infomation';
import MenuScreen from './components/menu';
const options = {
  header: null
};
const Home = createBottomTabNavigator({
  HomeSc:
    {
      screen: HomeScreen,
     

    },
  History:{
    screen: HistoryScreeen,
  },
  News: NewsScreen,
  User: UserScreen,
  Infomation: InfomationScreen
},
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === 'HomeSc') {
          iconName = `ios-cart${focused ? '' : '-outline'}`;
        } else if (routeName === 'History') {
          iconName = `ios-calendar${focused ? '' : '-outline'}`;
          // isSetting = true;
        } else if (routeName === 'News') {
          iconName = `ios-notifications${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'User') {
          iconName = `ios-contact${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Infomation') {
          iconName = `ios-help-circle${focused ? '' : '-outline'}`;
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#003366',
      inactiveTintColor: 'gray',
    },
  })

const RootStack = createStackNavigator(
  {
    Home: Home,
    // History:HistoryScreeen
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      // headerStyle: {
      //   backgroundColor: '#fff',
      //   height: 80
      // },
      // headerTintColor: '#fff',
      // headerTitleStyle: {
      //   fontWeight: 'bold',

      // },
     
      // headerTitle: <LogoTitle {...this.props} />
    header:null
    },
  }
);

const AppRoot = createDrawerNavigator(
  {
    Tab: RootStack
  }, {
    drawerWidth: 280,
    contentComponent: (props) => <MenuScreen />
  })

export default class App extends React.Component {
  render() {
    return <AppRoot />;
  }
}
