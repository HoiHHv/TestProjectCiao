import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
class ItemCart extends Component {
  constructor() {
    super()
  }
  render() {
    return (
      <View style={{ height: 110, flexDirection: 'row', borderBottomColor: 'grey', borderBottomWidth: 1 }}>
        <View style={{ height: '100%', width: 110, justifyContent: 'center', alignItems: 'center', }}>
          <Image source={require('../../assets/img/kem.jpg')} style={{ height: 100, width: 100, borderRadius: 5, }} />
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 2 }}>
            <Text style={{color:'black',fontWeight:'bold',paddingTop:5}}>Cafe sữa đá </Text>
            <Text>Đậm đà nhiều sữa</Text>
          </View>
          <View style={{ flex: 1 }}>

          </View>
          <View style={{ flex: 2, flexDirection: 'row' }}>
            <View style={{ width: '80%' }}>
              <Text style ={{color:'red',fontWeight:'bold'}}>35.000 đ</Text>
              <Text>50.000</Text>
            </View>
            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../../assets/img/btn.png')} style={{ height: 24, width: 24 }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
export default ItemCart;